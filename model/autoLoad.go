package model

import (
	"github.com/garyburd/redigo/redis"

	"fmt"
)

func main() {
	loginUser := fmt.Sprintf("%s%d", LoginUser, 1)

	RedisConn := RedisPool.Get()
	defer RedisConn.Close()

	userBytes, err := redis.Bytes(RedisConn.Do("GET", loginUser))
	fmt.Println(err)
	fmt.Println(userBytes)

}