import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _15682262 = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)
const _60ab588d = () => import('../pages/vote/index.vue' /* webpackChunkName: "pages/vote/index" */).then(m => m.default || m)
const _3477046e = () => import('../pages/links.vue' /* webpackChunkName: "pages/links" */).then(m => m.default || m)
const _149f70c6 = () => import('../pages/about.vue' /* webpackChunkName: "pages/about" */).then(m => m.default || m)
const _123b8dcc = () => import('../pages/book/index.vue' /* webpackChunkName: "pages/book/index" */).then(m => m.default || m)
const _1d3c274f = () => import('../pages/rank/index.vue' /* webpackChunkName: "pages/rank/index" */).then(m => m.default || m)
const _9fd8d5b8 = () => import('../pages/timeline/index.vue' /* webpackChunkName: "pages/timeline/index" */).then(m => m.default || m)
const _5e2379a2 = () => import('../pages/admin/index.vue' /* webpackChunkName: "pages/admin/index" */).then(m => m.default || m)
const _3aac8042 = () => import('../pages/signin.vue' /* webpackChunkName: "pages/signin" */).then(m => m.default || m)
const _4f42d6b8 = () => import('../pages/signup.vue' /* webpackChunkName: "pages/signup" */).then(m => m.default || m)
const _5cea7672 = () => import('../pages/book/create.vue' /* webpackChunkName: "pages/book/create" */).then(m => m.default || m)
const _b48b08c6 = () => import('../pages/verify/mail.vue' /* webpackChunkName: "pages/verify/mail" */).then(m => m.default || m)
const _444e3722 = () => import('../pages/ac/pwdReset.vue' /* webpackChunkName: "pages/ac/pwdReset" */).then(m => m.default || m)
const _4717f85e = () => import('../pages/vote/create.vue' /* webpackChunkName: "pages/vote/create" */).then(m => m.default || m)
const _4ac38220 = () => import('../pages/admin/setting.vue' /* webpackChunkName: "pages/admin/setting" */).then(m => m.default || m)
const _79384f7c = () => import('../pages/topic/create.vue' /* webpackChunkName: "pages/topic/create" */).then(m => m.default || m)
const _e7f7296c = () => import('../pages/ac/pwdModify.vue' /* webpackChunkName: "pages/ac/pwdModify" */).then(m => m.default || m)
const _c0e026d8 = () => import('../pages/admin/pushLink.vue' /* webpackChunkName: "pages/admin/pushLink" */).then(m => m.default || m)
const _d5764d44 = () => import('../pages/user/edit.vue' /* webpackChunkName: "pages/user/edit" */).then(m => m.default || m)
const _27d83846 = () => import('../pages/admin/user/today.vue' /* webpackChunkName: "pages/admin/user/today" */).then(m => m.default || m)
const _23dc538e = () => import('../pages/admin/crawl/custom.vue' /* webpackChunkName: "pages/admin/crawl/custom" */).then(m => m.default || m)
const _494bf372 = () => import('../pages/admin/topic/yesterday.vue' /* webpackChunkName: "pages/admin/topic/yesterday" */).then(m => m.default || m)
const _008748de = () => import('../pages/admin/topic/list.vue' /* webpackChunkName: "pages/admin/topic/list" */).then(m => m.default || m)
const _02ad5da0 = () => import('../pages/admin/crawl/zhihu.vue' /* webpackChunkName: "pages/admin/crawl/zhihu" */).then(m => m.default || m)
const _483e91ac = () => import('../pages/admin/reply/today.vue' /* webpackChunkName: "pages/admin/reply/today" */).then(m => m.default || m)
const _be2a3542 = () => import('../pages/admin/category/list.vue' /* webpackChunkName: "pages/admin/category/list" */).then(m => m.default || m)
const _65ba2d7c = () => import('../pages/admin/crawl/jianshu.vue' /* webpackChunkName: "pages/admin/crawl/jianshu" */).then(m => m.default || m)
const _d53830c4 = () => import('../pages/admin/user/yesterday.vue' /* webpackChunkName: "pages/admin/user/yesterday" */).then(m => m.default || m)
const _745cbb95 = () => import('../pages/admin/crawl/account.vue' /* webpackChunkName: "pages/admin/crawl/account" */).then(m => m.default || m)
const _283a0c9e = () => import('../pages/admin/topic/today.vue' /* webpackChunkName: "pages/admin/topic/today" */).then(m => m.default || m)
const _728b2d9c = () => import('../pages/admin/user/list.vue' /* webpackChunkName: "pages/admin/user/list" */).then(m => m.default || m)
const _1909c842 = () => import('../pages/admin/crawl/huxiu.vue' /* webpackChunkName: "pages/admin/crawl/huxiu" */).then(m => m.default || m)
const _aea974fa = () => import('../pages/admin/reply/list.vue' /* webpackChunkName: "pages/admin/reply/list" */).then(m => m.default || m)
const _5dd515ed = () => import('../pages/admin/reply/yesterday.vue' /* webpackChunkName: "pages/admin/reply/yesterday" */).then(m => m.default || m)
const _cc3ca62e = () => import('../pages/book/edit/chapter/_id.vue' /* webpackChunkName: "pages/book/edit/chapter/_id" */).then(m => m.default || m)
const _7317ab4a = () => import('../pages/vote/edit/_id.vue' /* webpackChunkName: "pages/vote/edit/_id" */).then(m => m.default || m)
const _a0880caa = () => import('../pages/book/edit/_id.vue' /* webpackChunkName: "pages/book/edit/_id" */).then(m => m.default || m)
const _00e01231 = () => import('../pages/user/collect/_id.vue' /* webpackChunkName: "pages/user/collect/_id" */).then(m => m.default || m)
const _20108f96 = () => import('../pages/topic/edit/_id.vue' /* webpackChunkName: "pages/topic/edit/_id" */).then(m => m.default || m)
const _fb7f332c = () => import('../pages/topic/_id.vue' /* webpackChunkName: "pages/topic/_id" */).then(m => m.default || m)
const _3aaad998 = () => import('../pages/book/_id.vue' /* webpackChunkName: "pages/book/_id" */).then(m => m.default || m)
const _4b6d4216 = () => import('../pages/user/_id.vue' /* webpackChunkName: "pages/user/_id" */).then(m => m.default || m)
const _40896019 = () => import('../pages/user/_id/index.vue' /* webpackChunkName: "pages/user/_id/index" */).then(m => m.default || m)
const _0c3450f3 = () => import('../pages/user/_id/vote.vue' /* webpackChunkName: "pages/user/_id/vote" */).then(m => m.default || m)
const _bdee069e = () => import('../pages/user/_id/collect.vue' /* webpackChunkName: "pages/user/_id/collect" */).then(m => m.default || m)
const _56d17e31 = () => import('../pages/user/_id/reply.vue' /* webpackChunkName: "pages/user/_id/reply" */).then(m => m.default || m)
const _6f3fcb96 = () => import('../pages/vote/_id.vue' /* webpackChunkName: "pages/vote/_id" */).then(m => m.default || m)
const _47e9b164 = () => import('../pages/active/_id/_key.vue' /* webpackChunkName: "pages/active/_id/_key" */).then(m => m.default || m)
const _4a04f89c = () => import('../pages/ac/_id/_key.vue' /* webpackChunkName: "pages/ac/_id/_key" */).then(m => m.default || m)



const scrollBehavior = (to, from, savedPosition) => {
  // SavedPosition is only available for popstate navigations.
  if (savedPosition) {
    return savedPosition
  } else {
    let position = {}
    // If no children detected
    if (to.matched.length < 2) {
      // Scroll to the top of the page
      position = { x: 0, y: 0 }
    }
    else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
      // If one of the children has scrollToTop option set to true
      position = { x: 0, y: 0 }
    }
    // If link has anchor, scroll to anchor by returning the selector
    if (to.hash) {
      position = { selector: to.hash }
    }
    return position
  }
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/",
			component: _15682262,
			name: "index"
		},
		{
			path: "/vote",
			component: _60ab588d,
			name: "vote"
		},
		{
			path: "/links",
			component: _3477046e,
			name: "links"
		},
		{
			path: "/about",
			component: _149f70c6,
			name: "about"
		},
		{
			path: "/book",
			component: _123b8dcc,
			name: "book"
		},
		{
			path: "/rank",
			component: _1d3c274f,
			name: "rank"
		},
		{
			path: "/timeline",
			component: _9fd8d5b8,
			name: "timeline"
		},
		{
			path: "/admin",
			component: _5e2379a2,
			name: "admin"
		},
		{
			path: "/signin",
			component: _3aac8042,
			name: "signin"
		},
		{
			path: "/signup",
			component: _4f42d6b8,
			name: "signup"
		},
		{
			path: "/book/create",
			component: _5cea7672,
			name: "book-create"
		},
		{
			path: "/verify/mail",
			component: _b48b08c6,
			name: "verify-mail"
		},
		{
			path: "/ac/pwdReset",
			component: _444e3722,
			name: "ac-pwdReset"
		},
		{
			path: "/vote/create",
			component: _4717f85e,
			name: "vote-create"
		},
		{
			path: "/admin/setting",
			component: _4ac38220,
			name: "admin-setting"
		},
		{
			path: "/topic/create",
			component: _79384f7c,
			name: "topic-create"
		},
		{
			path: "/ac/pwdModify",
			component: _e7f7296c,
			name: "ac-pwdModify"
		},
		{
			path: "/admin/pushLink",
			component: _c0e026d8,
			name: "admin-pushLink"
		},
		{
			path: "/user/edit",
			component: _d5764d44,
			name: "user-edit"
		},
		{
			path: "/admin/user/today",
			component: _27d83846,
			name: "admin-user-today"
		},
		{
			path: "/admin/crawl/custom",
			component: _23dc538e,
			name: "admin-crawl-custom"
		},
		{
			path: "/admin/topic/yesterday",
			component: _494bf372,
			name: "admin-topic-yesterday"
		},
		{
			path: "/admin/topic/list",
			component: _008748de,
			name: "admin-topic-list"
		},
		{
			path: "/admin/crawl/zhihu",
			component: _02ad5da0,
			name: "admin-crawl-zhihu"
		},
		{
			path: "/admin/reply/today",
			component: _483e91ac,
			name: "admin-reply-today"
		},
		{
			path: "/admin/category/list",
			component: _be2a3542,
			name: "admin-category-list"
		},
		{
			path: "/admin/crawl/jianshu",
			component: _65ba2d7c,
			name: "admin-crawl-jianshu"
		},
		{
			path: "/admin/user/yesterday",
			component: _d53830c4,
			name: "admin-user-yesterday"
		},
		{
			path: "/admin/crawl/account",
			component: _745cbb95,
			name: "admin-crawl-account"
		},
		{
			path: "/admin/topic/today",
			component: _283a0c9e,
			name: "admin-topic-today"
		},
		{
			path: "/admin/user/list",
			component: _728b2d9c,
			name: "admin-user-list"
		},
		{
			path: "/admin/crawl/huxiu",
			component: _1909c842,
			name: "admin-crawl-huxiu"
		},
		{
			path: "/admin/reply/list",
			component: _aea974fa,
			name: "admin-reply-list"
		},
		{
			path: "/admin/reply/yesterday",
			component: _5dd515ed,
			name: "admin-reply-yesterday"
		},
		{
			path: "/book/edit/chapter/:id?",
			component: _cc3ca62e,
			name: "book-edit-chapter-id"
		},
		{
			path: "/vote/edit/:id?",
			component: _7317ab4a,
			name: "vote-edit-id"
		},
		{
			path: "/book/edit/:id?",
			component: _a0880caa,
			name: "book-edit-id"
		},
		{
			path: "/user/collect/:id?",
			component: _00e01231,
			name: "user-collect-id"
		},
		{
			path: "/topic/edit/:id?",
			component: _20108f96,
			name: "topic-edit-id"
		},
		{
			path: "/topic/:id?",
			component: _fb7f332c,
			name: "topic-id"
		},
		{
			path: "/book/:id",
			component: _3aaad998,
			name: "book-id"
		},
		{
			path: "/user/:id?",
			component: _4b6d4216,
			children: [
				{
					path: "",
					component: _40896019,
					name: "user-id"
				},
				{
					path: "vote",
					component: _0c3450f3,
					name: "user-id-vote"
				},
				{
					path: "collect",
					component: _bdee069e,
					name: "user-id-collect"
				},
				{
					path: "reply",
					component: _56d17e31,
					name: "user-id-reply"
				}
			]
		},
		{
			path: "/vote/:id",
			component: _6f3fcb96,
			name: "vote-id"
		},
		{
			path: "/active/:id?/:key?",
			component: _47e9b164,
			name: "active-id-key"
		},
		{
			path: "/ac/:id?/:key?",
			component: _4a04f89c,
			name: "ac-id-key"
		}
    ],
    fallback: false
  })
}
